# Cockpit reCAPTCHA

Add Google reCAPTCHA to your Cockpit Forms


## Usage

First obtain the appropriate keys for the type of reCAPTCHA you wish to integrate ([Google reCAPTCHA v2](https://www.google.com/recaptcha/admin) or [Google reCAPTCHA v3](https://g.co/recaptcha/v3)).

> ℹ️ So far this has only been tested for the *Google reCAPTCHA v2* "Checkbox" challenge.


### Installation

Download the [latest release as a .zip-file](https://gitlab.com/consulting-group-esslingen/cockpit-addons/cockpit-recaptcha/-/releases) and extract to `your-cockpit-root/addons` (e.g. `cockpit/addons/reCAPTCHA`).

> ℹ️ Currently there is no latest release but you can [download the current master branch](https://gitlab.com/consulting-group-esslingen/cockpit-addons/cockpit-recaptcha/-/archive/master/cockpit-recaptcha-master.zip) to try Cockpit reCAPTCHA-


### Configuration

***See below for an example configuration***

The following configuration options are available (in `config/config.yaml`):


##### Available Configuration Options

|*Key*|Default|Notes|
|---|---|---|
|`server_key`|-|**REQUIRED** - The secret server key you obtained during the Google reCAPTCHA setup.|
|`response_field`|`g-recaptcha-response`|The name of the field which contains the Google reCAPTCHA response.|
|`send_remote_ip`|`false`|Whether the users IP address should be sent to Google along with the reCAPTCHA verification.|
|`request_method`|-|Which RequestMethod should be used to verify with Google, available: `Post`, `Curl`, `CurlPost`, `Socket`, `SocketPost`. By default the library will use `Post`.|
|`expected_hostname`|-|Ensures the hostname matches - You must do this if you have disabled "Domain/Package Name Validation" for your credentials. [Learn more](https://developers.google.com/recaptcha/docs/domain_validation#security_warning).|
|`expected_action`|-|Ensures the action matches for the v3 API. See [Learn more](https://developers.google.com/recaptcha/docs/v3#actions)|
|`score_threshold`|-|Set a score threshold for responses from the v3 API. [Learn more](https://developers.google.com/recaptcha/docs/v3#interpreting_the_score)|
|`challenge_timeout`|-|Set a timeout (in seconds) between the user passing the reCAPTCHA and your server processing it.|

The only **required** option is `secret_key`, which is the secret server key provided by Google during setup.


##### Example configuration

```yaml
recaptcha:
    server_key: [Your Secret Server Key]
    send_remote_ip: true
    request_method: CurlPost
```


### Frontend

Follow the [integration guide on the developer site](https://developers.google.com/recaptcha/intro) to add the Google reCAPTCHA into your frontend.

Make sure that the response received by reCAPTCHA is sent along the regular form data with the name you set in the configuration (or the default `g-recaptcha-response`).


## Error Messages

In case the reCAPTCHA was not verified correctly, you will receive a reply with status code 500 and the following JSON response containing the error code(s): `{"errors":["timeout-or-duplicate"]}`.

See the [Google reCAPTCHA error code reference](https://developers.google.com/recaptcha/docs/verify#error_code_reference) for all error codes and their description.

❗ Additionally, Cockpit reCAPTCHA can return three more error codes:

|Error code|Description|
|---|---|
|`missing-cockpit-config`|The required configuration options were not found in `config/config.yaml`.|
|`invalid-cockpit-config`|The configuration options provided in `config/config.yaml` are invalid.|
|`connection-failed`|There was an error while connecting to the verification endpoint. (Try changing your `request_method`).|

To resolve these issues, refer to the [Configuration section](#configuration).


## TODO

- [ ] Test against all available *Google reCAPTCHA v2* and *Google reCAPTCHA v3* variants
- [ ] Add options to choose for each form if you want to enable Cockpit reCAPTCHA
- [ ] Possibility to set the configuration options for each form


## Thanks

* To [Agentejo](https://agentejo.com) for creating Cockpit!
* To the contributors of [github.com/google/recaptcha](https://github.com/google/recaptcha), as this Addon is basically just a wrapper for it.


## Copyright and License

Copyright 2020 Consulting Group Esslingen e.V. under the MIT license.
