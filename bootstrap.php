<?php
/**
 *	Cockpit reCAPTCHA
 *  bootstrap.php
 *	Created on 2020-11-28 08:58
 *
 *  @author Consulting Group Esslingen e.V.
 *  @copyright Copyright (c) 2020 cg-esslingen.de
 *  @license MIT
 *
 */

require_once __DIR__ . '/vendor/autoload.php';

$this->module('recaptcha')->extend([
  'getConfig' => function() {

    static $config;

    if (isset($config)) return $config;

    $config = [
      'server_key'        => '',
      'send_remote_ip'    => false,
      'expected_hostname' => '',
      'expected_action'   => '',
      'score_threshold'   => -1,
      'challenge_timeout' => -1,
      'response_field'    => 'g-recaptcha-response',
      'request_method'    => null,
    ];

    $config = array_replace_recursive(
      $config,
      $this->app->retrieve('config/recaptcha')
    );

    return $config;
  },
]);

// get config variables
$config = $this->module('recaptcha')->getConfig();

$app->on('forms.submit.before', function($form, &$data, $frm, &$options) use ($config) {

  // checking the config
  if (!$config || !isset($config['secret_key'])) $this->stop(['errors' => ['missing-cockpit-config']], 500);
  // TODO: check for invalid values, then either return 'invalid-cockpit-config' or continue
  $responseField = $config['response_field'];

  // set request method
  switch(strtolower($config['request_method'])) {
    case 'curl':
      $requestMethod = new \ReCaptcha\RequestMethod\Curl();
      break;
    case 'curlpost':
      $requestMethod = new \ReCaptcha\RequestMethod\CurlPost();
      break;
    case 'socket':
      $requestMethod = new \ReCaptcha\RequestMethod\Socket();
      break;
    case 'socketpost':
      $requestMethod = new \ReCaptcha\RequestMethod\SocketPost();
      break;
    case 'post':
      $requestMethod = new \ReCaptcha\RequestMethod\Post();
      break;
    default:
      $requestMethod = null; // let the ReCaptcha library decide
  }

  // init ReCaptcha
  $recaptcha = new \ReCaptcha\ReCaptcha($config['secret_key'], $requestMethod);

  // prepare verification
  if ($config['expected_hostname'] != '') $recaptcha = $recaptcha->setExpectedHostname($config['expected_hostname']);
  if ($config['expected_action'] != '') $recaptcha = $recaptcha->setExpectedAction($config['expected_action']);
  if ($config['score_threshold'] > -1) $recaptcha = $recaptcha->setScoreThreshold($config['score_threshold']);
  if ($config['challenge_timeout'] > -1) $recaptcha = $recaptcha->setChallengeTimeout($config['challenge_timeout']);

  // execute verification
  $response = $recaptcha->verify(
    $data[$responseField],
    $config['send_remote_ip'] ? $_SERVER['REMOTE_ADDR'] : NULL
  );

  // check verification status
  if ($response->isSuccess() !== true) {
    $return = ['errors' => $response->getErrorCodes()];

    // throw Exception when using cockpit as library
    if (!COCKPIT_API_REQUEST && !COCKPIT_ADMIN) {
      throw new Exception(json_encode($return));
    }

    $this->stop($return, 500);
  }
});